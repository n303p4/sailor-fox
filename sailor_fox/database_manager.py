"""Database table manager."""

import json

from sqlitedict import SqliteDict


class DatabaseManager:
    """Simple class that handles a database.."""

    def __init__(self, filepath: str = "database.sqlite"):
        self.filepath = filepath
        self.tables = {}

    def table(self, tablename: str = None):
        """Load an SqliteDict database."""
        if tablename in self.tables:
            return self.tables[tablename]
        self.tables[tablename] = SqliteDict(
            self.filepath, tablename=tablename, encode=json.dumps, decode=json.loads, autocommit=True
        )
        return self.tables[tablename]
