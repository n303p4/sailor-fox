"""Subclass sailor Processor."""

import json

import sailor

from sailor_fox.database_manager import DatabaseManager


class ProcessorWithConfig(sailor.commands.Processor):
    """Subclass of Processor with configuration."""

    def __init__(self, *args, **kwargs):
        _database_filepath = kwargs.get("database_filepath", "database.sqlite")
        self._database_manager = DatabaseManager(_database_filepath)
        if "database_filepath" in kwargs:
            del kwargs["database_filepath"]
        super(ProcessorWithConfig, self).__init__(*args, **kwargs)
        self.description = "This is a bot."
        self.config = {}

    @property
    def database(self):
        return self._database_manager

    def load_config(self, filename: str = "config.json"):
        """Load configuration."""
        with open(filename, "r", encoding="utf-8") as file_object:
            self.config = json.load(file_object)

        self.name = self.config.get("name", self.name)
        self.description = self.config.get("description", self.description)

    def save_config(self, filename: str = "config.json"):
        """Save configuration."""
        with open(filename, "w", encoding="utf-8") as file_object:
            json.dump(self.config, file_object, indent=4)
