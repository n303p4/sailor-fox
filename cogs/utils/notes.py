"""User notes, using the database backend."""

# pylint: disable=C0103

from sailor import commands
from sailor.exceptions import UserInputError, FeatureNotImplemented

from sailor_fox.helpers import FancyMessage


def get_author_id(event):
    """Check to require author_id"""
    author_id = event.author_id
    if not author_id:
        raise FeatureNotImplemented(message="User ID support")
    return author_id


def get_table(event):
    """A generic way of getting the table for a user's notes."""
    return event.processor.database.table(f"{__name__}_{get_author_id(event)}")


@commands.cooldown(6, 12)
@commands.command(aliases=["n"])
async def note(event, note_name: str = None):
    """Note commands. Save and load notes for your user account.

    * Notes are limited to 1000 characters each.
    * Your account is limited to 20 notes maximum.
    """
    if note_name:
        await note.all_commands["open"].invoke(event, note_name)
    else:
        try:
            await event.processor.all_commands["help"].invoke(event, "note")
        except Exception:
            await event.reply(f"Run {event.f.monospace('help note')} for more details.")


@commands.cooldown(6, 12)
@note.command(name="list", aliases=["l"])
async def list_(event):
    """List all notes associated with your user account."""

    table = get_table(event)

    message = FancyMessage(event.f)
    message.add_line(event.f.bold("Your notes"))

    note_names = list(table.keys())
    if not note_names:
        await event.reply("You don't have any notes.")
        return
    for name in note_names:
        message.add_line(name)

    await event.reply(message)


@commands.cooldown(6, 12)
@note.command(aliases=["s"])
async def save(event, note_name: str, *, note_contents: str):
    """Create a new note associated with your user account.

    If a note with the same name already exists, it will be overwritten automatically.

    `note save mynote I have a delicious cheesecake`

    * Notes are limited to 1000 characters each.
    * Your account is limited to 20 notes maximum.
    """

    table = get_table(event)
    if len(table) >= 20:
        await event.reply("You may only store up to 20 notes at once.")
        return
    if len(note_contents) > 1000:
        await event.reply("Notes can only be up to 1000 characters.")
        return
    if not note_contents.strip():
        await event.reply("Note cannot be only whitespace!")
        return
    table[note_name] = note_contents
    await event.reply(f"Saved note {event.f.monospace(note_name)}")


@commands.cooldown(6, 12)
@note.command(aliases=["o"])
async def open(event, *, note_name: str):
    """Display a note associated with your user account.

    `note open mynote`

    Shorthand: If your note is not called `open`, then you can just use `note mynote` instead.
    """

    table = get_table(event)
    note_contents = table.get(note_name)
    if note_contents:
        await event.reply(note_contents)
        return
    await event.reply(f"Note {event.f.monospace(note_name)} does not exist.")


@commands.cooldown(6, 12)
@note.command(aliases=["d", "del"])
async def delete(event, *, note_name: str):
    """Delete a note associated with your user account.

    `note delete mynote`
    """

    table = get_table(event)
    if note_name in table:
        del table[note_name]
        await event.reply(f"Deleted note {event.f.monospace(note_name)}")
        return
    await event.reply(f"Note {event.f.monospace(note_name)} does not exist.")
