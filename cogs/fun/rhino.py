"""An assorted collection of meme commands."""

# pylint: disable=invalid-name

import secrets

import sailor

SUMMONABLES = [
    ("Red-Eyes Black Dragon", "https://files.catbox.moe/0pxh05.png"),
    ("Blue-Eyes White Dragon", "https://files.catbox.moe/wzwth3.png"),
    ("Exodia the Forbidden One", "https://files.catbox.moe/m87fze.png"),
    ("Fox Fire", "https://files.catbox.moe/uqlmpm.png"),
    ("Bujingi Fox", "https://files.catbox.moe/1kx13e.png"),
    ("Lunalight Crimson Fox", "https://files.catbox.moe/54bkq2.png"),
    ("Majespecter Fox - Kyubi", "https://files.catbox.moe/wyzvwi.png")
]
PLAY_MESSAGES = [
    "Play with me? o.o",
    "Yay, let's play! Try using the help command for a list of commands~ :3"
]


@sailor.commands.cooldown(6, 12)
@sailor.command()
async def play(event):
    """Play a game!
    
    This command is a parody of RhinoBot: The music bot for Discord.
    """
    message = secrets.choice(PLAY_MESSAGES)
    await event.reply(message)


@sailor.commands.cooldown(6, 12)
@sailor.command(name="np", aliases=["noproblem"])
async def np_(event):
    """No problem!

    This command is a parody of RhinoBot: The music bot for Discord.
    """
    await event.reply("No problem! :3")


@sailor.commands.cooldown(6, 12)
@sailor.command()
async def pause(event):
    """Pause for a bit.

    This command is a parody of RhinoBot: The music bot for Discord.
    """
    await event.reply("...")
    await event.sleep(5)
    await event.reply("...? o.o")


@sailor.commands.cooldown(6, 12)
@sailor.command()
async def summon(event):
    """Summon a monster!

    This command is a parody of RhinoBot: The music bot for Discord.
    """
    choice = secrets.choice(SUMMONABLES)
    name = choice[0]
    image = choice[1]
    await event.reply(f"I summon {name}!\n{image}")
