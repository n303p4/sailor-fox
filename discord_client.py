"""
A Discord client for server.py

Requires Python 3.8+ and discord.py 2.1.0 or higher.
"""

# pylint: disable=invalid-name,broad-except

import asyncio
from datetime import datetime, timedelta, timezone
import json

import aiohttp
import discord
from discord.ext import commands

from sailor_fox.helpers import create_logger, to_one_liner

logger = create_logger("discord")


async def get_last_post_by(channel, user):
    """Retrieve the last thing posted in a channel by user.

    Returns the `Message` object it found, or `None` if nothing is found.

    * `channel` - The channel to search for images in.
    * `user` - The user to match.
    """
    message = None
    async for message in channel.history():
        if message.author == user:
            return message


async def delete_message_within(message, interval: int = 5):
    """Delete a message that was sent within interval # of minutes."""
    now = datetime.now(timezone.utc)
    if now - message.created_at < timedelta(minutes=interval):
        await message.delete()
        return
    warning = await message.channel.send("Can only undo a post from within the last 5 minutes.")
    await warning.delete(delay=8)


async def do_action(action: dict, ctx: commands.Context = None, *, is_error: bool = False):
    """Perform a single action requested by the server."""
    message = ctx.message
    log_message = f"id={message.id} actionType={action['type']}"
    value = action["value"]
    if action["type"] == "rename_channel":
        try:
            channel_old_name = message.channel.name if hasattr(message.channel, "name") else "N/A"
            log_message += (
                f" channelId={message.channel.id}"
                f" channelOldName={channel_old_name}"
                f" channelNewName={to_one_liner(value)}"
            )
            if not isinstance(message.channel, discord.TextChannel):
                log_message += (
                    f" channelType={type(message.channel).__name__}"
                )
                logger.warning("%s | Can't rename this type of channel", log_message)
                return
            await message.channel.edit(name=value)
        except discord.HTTPException as error:
            logger.warning("%s | Rename failed: %s", log_message, error)
        else:
            logger.info("%s | Rename succeeded", log_message)
    elif action["type"] == "reply":
        log_message += f" | {to_one_liner(value)}"
        if is_error:
            logger.warning(log_message)
        else:
            logger.info(log_message)
        await ctx.send(value)
    elif action["type"] == "sleep":
        log_message += f" | Sleeping for {value} seconds..."
        logger.info(log_message)
        await asyncio.sleep(value)
    else:
        logger.warning("%s | Unsupported action", log_message)


def main():
    """Factory to create and start the bot."""

    with open("config.json") as config_file:
        config = json.load(config_file)

    assert "port_number" in config, "port_number must be set."
    port_number = config["port_number"]
    assert (isinstance(port_number, int)), "port_number must be an integer."

    main_prefix = config.get("prefix", "")

    assert "discord_slash_prefix" in config, "discord_slash_prefix must be set."
    discord_slash_prefix = config["discord_slash_prefix"]
    assert isinstance(discord_slash_prefix, str), "discord_slash_prefix must be a string."
    assert len(discord_slash_prefix) > 0, "discord_slash_prefix must be at least one character long."

    owner_ids = config.get("discord_owner_ids", [])
    assert isinstance(owner_ids, list), "discord_owner_ids must be a list."

    intents = discord.Intents.default()
    intents.message_content = True

    bot = commands.AutoShardedBot(
        command_prefix=commands.when_mentioned_or(main_prefix),
        intents=intents, help_command=None
    )
    bot.http_client = None

    @bot.event
    async def on_ready():
        """Set the bot's playing status to the help command."""
        bot.http_client = aiohttp.ClientSession(loop=bot.loop)
        game = discord.Game(
            name=f"/{discord_slash_prefix} help | {main_prefix}help | @{bot.user.name} help"
        )
        await bot.change_presence(activity=game)

    @bot.event
    async def on_command_error(ctx, exc):
        """If a command was not found, then use slash_command."""
        if not isinstance(exc, commands.CommandNotFound):
            logger.error(exc)
            return
        await ctx.invoke(
            bot.all_commands[discord_slash_prefix],
            input=ctx.message.content.replace(ctx.prefix, "", 1).lstrip()
        )

    @bot.hybrid_command(aliases=["z"])
    @commands.cooldown(4, 1)
    async def undo(ctx):
        """
        Remove the last thing that was posted by the bot.

        Only works on messages that are within 5 minutes old.
        """
        await ctx.defer()
        if not config.get("discord_enable_undo"):
            await ctx.send("Undo is currently disabled.", delete_after=8)
            return
        if not ctx.interaction:
            try:
                await ctx.message.delete()
            except discord.HTTPException:
                pass
        await delete_message_within(await get_last_post_by(ctx.channel, ctx.bot.user), 5)
        if ctx.interaction:
            await delete_message_within(await get_last_post_by(ctx.channel, ctx.bot.user), 5)

    @bot.hybrid_command(name=discord_slash_prefix)
    @commands.cooldown(4, 1)
    async def slash_command(ctx, *, input):
        """The main command, which forwards commands to the sailor-fox server."""
        await ctx.defer()
        await on_slash_command(ctx, input)

    async def on_slash_command(ctx: commands.Context, message_text: str):
        message = ctx.message
        application_info = await bot.application_info()
        is_owner = (
            message.author.id == application_info.owner.id or message.author.id in owner_ids
        )

        logger.info(
            "id=%s user=%s userId=%s guild=%s guildId=%s | %s",
            message.id,
            message.author,
            message.author.id,
            message.guild.name if message.guild else None,
            message.guild.id if message.guild else None,
            to_one_liner(message_text)
        )

        request_body = {
            "id": f"discord_client.py:{message.id}",
            "author_id": f"discord:{message.author.id}",
            "message": message_text,
            "is_owner": is_owner,
            "character_limit": 2000,
            "format_name": "discord",
            "channel_name": message.channel.name if isinstance(message.channel, discord.TextChannel) else None
        }

        try:
            async with bot.http_client.post(
                f"http://localhost:{port_number}",
                json=request_body,
                timeout=10
            ) as response:
                action_stack = await response.json()
            if response.status == 404:  # remain silent on 404 (CommandNotFound)
                if ctx.interaction:
                    await ctx.send(".", delete_after=0.001)
                return
            is_error = response.status != 200
            for action in action_stack:
                try:
                    await do_action(action, ctx, is_error=is_error)
                except Exception as error:
                    logger.warning("id=%s action=%s error=%s | Unsupported action",
                                   ctx.message.id, action, error)
            if ctx.interaction and not action_stack:
                await ctx.send(".", delete_after=0.001)

        except Exception as error:
            logger.error("id=%s | %s", message.id, to_one_liner(str(error)))
            if isinstance(error, aiohttp.client_exceptions.ClientConnectorError):
                await ctx.send("My brain stopped working. Please contact my owner. :<")
            elif not bot.http_client:
                await ctx.send("Still waking up; hold tight. :3")
            else:
                await ctx.send(str(error))

    assert "discord_token" in config, "discord_token must be set."
    discord_token = config["discord_token"]
    assert (isinstance(discord_token, str)), "discord_token must be a string."
    bot.run(discord_token)


if __name__ == "__main__":
    main()
