# cogs.utils.color

Contains a cog that fetches colors.

Note: Arguments enclosed in `[]` are optional.

## `color`

**Aliases:** `colour`

**Arguments:** `[color=None]`

Display a color, with detailed info. Accepts CSS color names and hex input.

* `color` - Either a CSS color or hex input. If not provided, a random color will be used.

## `simplecolor`

**Aliases:** `simplecolour, scolor, scolour`

**Arguments:** `[color=None]`

Display a color, without detailed info. Accepts CSS color names and hex input.

* `color` - Either a CSS color or hex input. If not provided, a random color will be used.