# cogs.lookup.dictionary

Dictionary lookup command.

Note: Arguments enclosed in `[]` are optional.

## `define`

**Aliases:** `def`

**Arguments:** `word`

Define a word.

**Example usage**

* `define cat`
* `define dog`
* `define fox`